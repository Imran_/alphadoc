﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpha.User.Models;

namespace Alpha.User.Model.Paitent
{
    public class PaitentProfile 
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public virtual UserTable User { get; set; }
        public string Name { get; set; }
        public string DpUrl { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public int Age { get; set; }

       public Tuple <string,bool> EventChange(PaitentProfile oldVale)
        {
            if (oldVale.Height != this.Height)
                return new Tuple<string, bool>("Height Update form "+oldVale.Height+" to "+this.Height,true);
            if (oldVale.Weight != this.Weight)
                return new Tuple<string, bool>("Weight Update form "+oldVale.Weight+" to "+this.Weight,true);

            return new Tuple<string, bool>("No Updates", false);
        }
    }
}
