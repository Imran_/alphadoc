﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alpha.User.Models;

namespace Alpha.User.Model.Doctor
{
    public class DoctorProfile
    {
        [Key]
        public long Id { get; set; }
        public string UserId { get; set; }
        public virtual UserTable User { get; set; }
        public  string Name { get; set; }
        public string Qualification { get; set; }
        public string LicesnceNumber { get; set; }
        public string Picture { get; set; }
        public string DS { get; set; }

        public string Avaialble { get; set; }
        public string TimeAvlble { get; set; }
    }
}
