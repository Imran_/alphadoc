﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Alpha.User.Model.Doctor;
using Alpha.User.Models;

namespace Alpha.Doctor.Portal.Controllers
{
    [Authorize(Roles ="doctor")]
    public class ProfileController : Controller
    {

        private MedicContext dc = new MedicContext();
        // GET: Profile
        public ActionResult Index()
        {
            var user = User.Identity.Name;
            var userId = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
            DoctorProfile profile = dc.DoctorProfiles.FirstOrDefault(i => i.UserId == userId);
            if (profile==null)
            {
                profile = new DoctorProfile
                {
                    UserId = userId,
                    Picture = "http://lorempixel.com/400/200"
                };
                dc.DoctorProfiles.Add(profile);
                dc.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(profile);
        }
        public ActionResult Edit()
        {
            var user = User.Identity.Name;
            var userId = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
            DoctorProfile profile = dc.DoctorProfiles.FirstOrDefault(i => i.UserId == userId);
            if (profile == null)
                return PartialView();
            return PartialView(profile);
        }

        [HttpPost]
        public ActionResult Edit(DoctorProfile model)
        {
            if(ModelState.IsValid)
            {
                dc.Entry(model).State = EntityState.Modified;
                dc.SaveChanges();
                TempData["Sucess"] = "Data Saved!";
            }
            else
            {
                TempData["Error"] = "Something went Wrong!";
            }
            return RedirectToAction("Index");
        }



        public ActionResult Appointments(int page = 0, int size = 5)
        {
            var user = User.Identity.Name;
            var userId = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
            var r = dc.Appointments.Include(i => i.Doctor).Where(i => i.DoctorId == userId).OrderByDescending(i => i.On).Skip(page * size).Take(size).ToList();
            return PartialView(r);

        }
        public ActionResult Events(int page = 0, int size = 5)
        {
            var user = User.Identity.Name;
            var userId = dc.Users.FirstOrDefault(i => i.UserName == user).Id;
            var r = dc.DoctorsActivities.Where(i => i.UserTableId == userId).OrderByDescending(i => i.On).Skip(page * size).Take(size).ToList();
            return PartialView(r);

        }

        public string Name(string Id)
        {
            return dc.PatientProfiles.FirstOrDefault(i => i.UserId == Id)?.Name;
        }
    }
}