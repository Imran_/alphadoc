﻿$(function () {
    var UserOnline=[];
    var isChrome = !!navigator.webkitGetUserMedia;
    var mediaConstraints = { "audio": true, "video": true };
    var STUN = { url: isChrome ? 'stun:stun.l.google.com:19302' : 'stun:23.21.150.121' };
    var iceServers = { iceServers: [STUN] };
    var sdpConstraints = {
        'mandatory': {
            'OfferToReceiveAudio': true,
            'OfferToReceiveVideo': true
        }
    };
    var DtlsSrtpKeyAgreement = { DtlsSrtpKeyAgreement: true };
    var optional = { optional: [DtlsSrtpKeyAgreement] };
    var connection = new RTCPeerConnection(iceServers, optional);

    var signal = $.connection.webrtchub;

    signal.client.broadcastMessage = function (from, message) {
       
        if (from != name) {
            
            remoteSDP = new RTCSessionDescription(JSON.parse(message));
            connection.setRemoteDescription(remoteSDP, function () {   
            }, function (e) {
               $("#myModalBody").html("Calling Failed:" +e)
            });
        }
    }

    

    var myVideo = null;
    var RemoteVideo = null;
    connection.onaddstream = function (evt) {
        attachMediaStream(remoteVideo, evt.stream);
    };

    var OnICE = function (event) {
        var candidate = event.candidate;
        if (candidate === null) {
            send_SDP();
        }
    };

    function send_SDP() {
        sendMessage(JSON.stringify(connection.localDescription));
    }

    var getSDP = function (obj) {
        console.log(obj);
        connection.setLocalDescription(obj);
    }

    function gotLocalStream(stream) {
        attachMediaStream(localVideo, stream);
        connection.addStream(stream);
        connection.onicecandidate = OnICE;
        connection.createOffer(getSDP, function () { alert("could not create sdp"); }, sdpConstraints);
    }
    function Unbind() {
        $("li.Call").unbind("click");
        $("li.Call").click(function () {
            var caller = $(this).data("caller");
            var callee = $(this).data("callee");
            $("#myModalBody").html("Calling " + callee)


            $("#myModal").modal();
            Unbind();
        });
    }
    Unbind();
    $.connection.hub.start({ transport: ['longPolling'] }).done(function () {
        ready = true;
        signal.server.send(name, message);
        signal.server.getConnectedUsers().done(function (users) {
            $.each(users, function (i, username) {
                UserOnline.push(username);

            });
            insertUsers();
        });
        
    });

    var insertUsers = function () {
        var str = "";
        $.each(UserOnline, function (i, username) {
            str += "<li class='Call' data-user='" + username.Username + "' data-conectionid='" + username.ConnectionId + "' > " +
                                                   " <a href='#'>" + username.Username + "</a>" +
                        " </li>";

        });
        $("#Sdr").html(str);
        Unbind();
    }
    var sendMessage = function (message) {
        
        if (!ready)
            setTimeout(sendMessage, 100, message);
        else {
            signal.server.send(name, message);
        }
    }
})