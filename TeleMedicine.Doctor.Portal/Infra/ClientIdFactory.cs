﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Microsoft.AspNet.SignalR.Infrastructure

namespace Apha.Doctor.Portal.Infra
{
    public class ClientIdFactory : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            if (request.User.Identity.IsAuthenticated)
            {
                return request.User.Identity.Name;
            }
            else return Guid.NewGuid().ToString();
        }
    }
}