﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alpha.Doctor.Portal.Models
{
    public class VideoUser
    {
        public string Username;
        public string ConnectionId;
        public bool InCall;
    }
    public class CallOffer
    {
        public VideoUser Caller;
        public VideoUser Callee;
    }
    public class UserCall
    {
        public List<VideoUser> Users;
    }
}